set tabstop=2 softtabstop=2 shiftwidth=2
set expandtab
set nofoldenable
set colorcolumn=80
set spell
syntax on

set textwidth=80

"au BufEnter *.hs compiler ghc
au BufEnter *.fs set syntax=fs
au BufEnter *.asmx set syntax=cs

set number
set listchars=tab:·\ ,eol:†
map gs :%s
" map t ::tabnew 


" Latex configuration
"filetype plugin on
"set grepprg=grep\ -nH\ $*
"filetype indent on
"let g:tex_flavor='latex'

"noremap <C-l> :w<CR>:!pdflatex %<CR>
"noremap <C-e> :!evince $(basename % .tex).pdf >/dev/null 2>&1 &<CR>
noremap <C-l> :!pdflatex $(p=`ls *.pdf`; b=$(basename $p .pdf); if [ 1 == `echo $p \| wc -l` ] && [ -e $b.tex ]; then echo $b; else basename % .tex; fi).tex<CR>
noremap <C-e> :!evince $(p=`ls *.pdf`; b=$(basename $p .pdf); if [ 1 == `echo $p \| wc -l` ] && [ -e $b.tex ]; then echo $b; else basename % .tex; fi).pdf >/dev/null 2>&1 &<CR>

noremap <C-p> :w<CR>:!latex %<CR>:!dvipdf $(basename % .tex).dvi<CR>
