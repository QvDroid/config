#!/bin/sh
cd $(dirname $0)
GIT_DIR=$(pwd)

find . \( -path ".*/.git" -prune -and -not -name ".git" \) \
       -or \( -type f -not -name "$(basename $0)" -and -not -name "*.swp" \) | \
  while read path; do
    dest="$HOME/$path"
    if [ -e $dest ]; then
      if [ -L $dest ]; then
        echo "Symlink for $path exists, skipping."
        continue
      else
        echo "backing up existing $dest to ${dest}_backup"
        mv $dest ${dest}_backup
      fi
    fi
    echo "creating symlink for $path."
    ln -s $GIT_DIR/$path $dest
  done
