
export EDITOR="vim"

function parse_git_branch {
  git branch --no-color 2> /dev/null | sed -e '/^[^*]/d' -e "s/* \(.*\)/$1/"
}

function init_promt {
  local        BLUE="\[\e[0;34m\]"
  local         RED="\[\e[0;31m\]"
  local   LIGHT_RED="\[\e[1;31m\]"
  local       GREEN="\[\e[0;32m\]"
  local LIGHT_GREEN="\[\e[1;32m\]"
  local       WHITE="\[\e[1;37m\]"
  local  LIGHT_GRAY="\[\e[0;37m\]"
  local  RESET="\[\e[0m\]"

  case $TERM in
    rxvt*)
    local T='┏'
    local B='┗'
    local M='━'
    local COLOR=$GREEN
    ;;
    *)
    local COLOR=$RESET
  esac

  PS1="\
$COLOR$T($GREEN\u@\h$COLOR)\$(parse_git_branch '$M($BLUE\1$COLOR)')$M($GREEN\w$COLOR)\n\
$COLOR$B$ $RESET"
  PS2="$COLOR> $RESET"
}

init_promt

alias ls='ls --color=auto'
#alias pacman='powerpill'
alias jdown='java -jar /home/lukas/.jd/JDownloader.jar'
alias ant='ant -quiet'

complete -cf sudo

#make backup
function bk {
	TODAY=$(date +%d-%m-%y)

	for x in $@
	do
		cp -R $x $x"_backup_"$TODAY
	done
}

function extract {
  taskset 01 unrar x $@
}

function trash {
	TODAY=$(date +%d-%m-%y)
	UserHome="/home/lukas"

	mkdir -p $UserHome/.trash/$TODAY

	for x in $@
	do
		if [[ -e $x ]]; then
			# get absolute path
			absPath=$(readlink -f $x)
		
			# test if file/directory x is in .trash
			if [[ -n $(echo $absPath | grep $UserHome/.trash/) ]] || [ $absPath = $UserHome/.trash ]; then
				# file is in the trash -> remove it
				rm $x -R
				if [ $? == 0 ]; then
					echo "Deleted $x"
				fi
			else
				# file is not in the trash -> move to trash
				res=$(mv $x $UserHome/.trash/$TODAY/ 2>&1)

				if [ $? == 0 ]; then
					echo "Moved $x to ~/.trash/$TODAY/"

				elif [[ -n $(echo $res | grep "Permission denied") ]]; then
					echo "$x: Permission denied"

				else
					echo $res
				fi
			fi
		else
			echo "$x: No such file or directory"
		fi
	done
}

#alias rm='trash'

# manpage colours
export LESS_TERMCAP_mb=$'\E[01;31m'
export LESS_TERMCAP_md=$'\E[01;31m'
export LESS_TERMCAP_me=$'\E[0m'
export LESS_TERMCAP_se=$'\E[0m'                           
export LESS_TERMCAP_so=$'\E[01;44;33m'                                 
export LESS_TERMCAP_ue=$'\E[0m'
export LESS_TERMCAP_us=$'\E[01;32m'

export SVN_EDITOR=vim
