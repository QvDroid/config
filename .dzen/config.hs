
--module Dzen.Config where
import Control.Applicative
import Control.Concurrent
import Control.Exception
import Control.Monad
import Data.List
import Data.Maybe
import System.IO
import System.IO.Error
import System.Locale
import System.Posix.Signals
import System.Process
import System.Time
import System.Timeout
import Text.Regex.Posix

iconPath     = "/home/l/.dzen/icons/"

padding      = "  |  "
leftPadding  = ""
rightPadding = "  "

acOnlineFile = "/sys/class/power_supply/AC/online"
batteryPath  = "/sys/class/power_supply/BAT0/"
batteryPresentFile     = batteryPath ++ "present"
batteryChargeFullFile  = batteryPath ++ "charge_full"
batteryChargeValueFile = batteryPath ++ "charge_now"

timeFormat = "%a %b %d  %H:%M"

-- |Type containing the state of the battery
data BatteryState = BatteryState {
  batteryPresent :: Bool,   -- ^ True if there is a battery plugged in.
  batteryChargeValue :: Int -- ^ Percentage of the battery that is charged.
}

-- |Type containing the state of the power adapter.
data AcState = AcState {
  adapterPresent :: Bool -- ^ True if the power adapter is plugged in.
}

-- |Type containing the state of the audio
data AudioState = AudioState {
  audioMuted :: Bool,  -- ^ True if the audio device is muted.
  audioVolume :: Int   -- ^ Volume of the audio device (in percent).
}

-- | Tries to convert a 'String' into a type of the class 'Read'.
saveRead :: (Read a) => String -> Maybe a
saveRead = fmap fst . listToMaybe . reads

eitherToMaybe :: Either b a -> Maybe a
eitherToMaybe (Left _) = Nothing
eitherToMaybe (Right x) = Just x

saveReadFile :: String -> IO (Maybe String)
saveReadFile file = eitherToMaybe <$> tryJust (guard . isDoesNotExistError) (readFile file)

-- | Parse the values read from the battery files and produce a 'BatteryState'
--   if possible.
parseBatteryState :: Maybe String  -- ^ Value of the 'batteryPresentFile'
                                   --   (should be either '0' or '1')
                  -> Maybe String  -- ^ Value of the 'batteryChargeFullFile'
                                   --   (should be an integer)
                  -> Maybe String  -- ^ Value of the 'batteryChargeValueFile'
                                   --   (should be an integer)
                  -> Maybe BatteryState
parseBatteryState p f n = liftM2 BatteryState batteryPresent chargeVal
  where 
    chargeVal = liftM2 calcPerc chargeFull chargeNow
    calcPerc f n = min 100 (100 * n `div` f)
    batteryPresent = liftM (==1) (p >>= saveRead) 
    chargeFull = f >>= saveRead :: (Maybe Int)
    chargeNow  = n >>= saveRead :: (Maybe Int)

-- | Parse the value read from the power adapter file and produce an 'AcState'
--   if possible
parseAcState :: String        -- ^ Value of the 'acOnlineFile' (should be either
                              --   '0' of '1')
             -> Maybe AcState
parseAcState o = liftM (AcState . (==1)) $ saveRead o

-- | Parse the value read from the audio command and produce an 'AudioState'
--   if possible
parseAudioState :: String -> Maybe AudioState
parseAudioState s = liftM2 AudioState mute volume
  where
    volume = nthElement 0 groups >>= saveRead
    mute   = liftM (== "off") $ nthElement 1 groups 
    groups = getMatchedGroups s "\\[([0-9]*)%\\].*\\[((off)|(on))\\]"



-- | Read the battery state files and try to convert their content to a
--   'BatteryState'
readBatteryState :: IO (Maybe BatteryState)
readBatteryState = do
    present <- saveReadFile batteryPresentFile
    chargeFull <- saveReadFile batteryChargeFullFile
    chargeNow <- saveReadFile batteryChargeValueFile
    return $ parseBatteryState present chargeFull chargeNow
{-
    return $
      present >>= \p ->
      chargeFull >>= \f ->
      chargeNow >>= \n ->
      parseBatteryState p f n
-}

-- | Read the power adapter state files and try to convert their content to a
--   'AcState'.
readAcState :: IO (Maybe AcState)
readAcState = do
    online <- saveReadFile acOnlineFile
    return $ parseAcState =<< online

-- | Read the audio state files and try to convert their content to an
--   'AudioState'.
readAudioState :: IO (Maybe AudioState)
readAudioState = do
    out <- readProcess "/usr/bin/amixer" ["get", "Master"] ""
    return $ parseAudioState out

-- | Match the given string against the given regex and return the matched
--   groups of the regex.
getMatchedGroups :: String    -- ^ The string to be search in.
                 -> String    -- ^ The regex.
                 -> [String]
getMatchedGroups s r = case s =~ r :: (String, String, String, [String]) of
  (_, _, _, x) -> x

-- | Retrieve the element at a given index in the list or 'Nothing' if the list is
--   smaller.
nthElement :: Int -> [a] -> Maybe a
nthElement 0 (x:_)  = Just x
nthElement _ []     = Nothing
nthElement n (_:xs) = nthElement (n-1) xs

-- | Read the current time in the format described by 'timeFormat'.
readTime :: IO String
readTime = do
  time <- getClockTime >>= toCalendarTime
  return $ formatCalendarTime defaultTimeLocale timeFormat time 


-- | Pack the name of an icon into a markup that dzen understands.
showIcon :: String -> String
showIcon name = "^i(" ++ iconPath ++ name ++ ")"

-- | Convert the power states to a 'String' displayed on the dzen bar.
showPower :: AcState -> BatteryState -> String
showPower a b = showIcon icon ++ " " ++ value
  where 
    value
      | batteryPresent b = show (batteryChargeValue b) ++ "%"
      | otherwise        = "AC"
    icon 
      | adapterPresent a && batteryPresent b = "power-ac.xbm"
      | adapterPresent a                     = "power-nobat.xbm"
      | batteryPresent b                     = "power-bat.xbm"
      | otherwise = "" -- there is no battery and no power-adapter
                       -- (how can the computer be running :P)

-- | Convert the audio state to a 'String' displayed on the dzen bar.
showAudio :: AudioState -> String
showAudio a = showIcon icon ++ " " ++ show (audioVolume a) ++ "%"
  where
    icon
      | audioMuted a       = "vol-m.xbm"
      | audioVolume a > 66 = "vol-hi.xbm"
      | audioVolume a > 33 = "vol-mid.xbm"
      | otherwise          = "vol-low.xbm"
      
-- | Main loop of the configuration.
--   Output the string obtained by it's argument every minute or whenevery a
--   SIGUSR1-signal is received
showPanel :: IO String -> IO ()
showPanel m = do
  sigVar <- newMVar ()
  installHandler sigUSR1 (Catch $ putMVar sigVar ()) Nothing
  loop $ m >>= putStrLn >> hFlush stdout >> waitNextEvent (takeMVar sigVar)
  where
    loop            = sequence_ . repeat
    waitNextEvent m = getClockTime >>= toCalendarTime >>= \c ->
                      timeout ((60 - ctSec c) * 1000000) m

-- | Run the main loop with all the data avaliable.
main :: IO ()
main = showPanel panelValue
  where
    panelValue = do
      battery <- readBatteryState
      ac <- readAcState
      audio <- readAudioState
      time <- readTime
      return $
        leftPadding ++
        intercalate padding [powerVal battery ac, audioVal audio, time] ++
        rightPadding

    powerVal :: Maybe BatteryState -> Maybe AcState -> String
    powerVal b a = maybe "Unavailable" (showPower ac) b
      where
        ac = fromMaybe (AcState False) a

    audioVal :: Maybe AudioState -> String
    audioVal = maybe "Unavailable" showAudio
