{-# LANGUAGE FlexibleContexts, FlexibleInstances, MultiParamTypeClasses,
    PatternGuards, DeriveDataTypeable #-}

import Control.Applicative
import Control.Arrow(second)
import Data.Maybe
import Test.QuickCheck
import System.Exit
import System.Posix.Unistd
import qualified Data.Map


import XMonad
import XMonad.Core
import XMonad.Actions.UpdatePointer
import XMonad.Hooks.DynamicLog
import XMonad.Hooks.ManageDocks
import XMonad.Hooks.ManageHelpers
import XMonad.Hooks.SetWMName
import XMonad.Layout.Fullscreen
import XMonad.Layout.Master
import XMonad.Layout.MessageControl
import XMonad.Layout.MultiToggle
import XMonad.Layout.MultiToggle.Instances
import XMonad.Layout.NoBorders
import XMonad.Layout.Reflect
import XMonad.Layout.Tabbed
import qualified XMonad.StackSet as S
import XMonad.Util.EZConfig


-- overall Config

myTerminal   = "urxvt"
myBrowser    = "firefox"
myLauncher   = "gmrun"

-- Host specific config
myHosts = [("eee", Host Small mod4Mask)
          ,("black", Host Large mod4Mask)
          ,("xmg", Host Large mod1Mask)
          ]
myDefaultHost = Host Large mod4Mask

-- Workspaces (Name, key binding)
myWorkspaces = [("main", "q")
               ,("W", "w")
               ,("E", "e")
               ,("R", "r")
               ,("U", "u")
               ,("I", "i")
               ,("O", "o")
               ,("P", "p")
               ]

-- Workspace Colors 

fgColorCurrent  = "#ffffff"
bgColorCurrent  = "#ee4000"
fgColorVisible  = "#ffffff"
bgColorVisible  = "#800000"
fgColorHidden   = "#ffffff"
bgColorHidden   = ""
fgColorEmpty    = "#555555"
bgColorEmpty    = ""


-- Data for host specific configs
data ScreenSize = Small | Large
  deriving (Read, Show, Eq)

data Host = Host
  {  screenSize :: ScreenSize
  ,  modifier :: KeyMask
  }
  deriving (Read, Show, Eq)

getHost :: IO Host
getHost = fmap nodeName getSystemID >>= \hostName ->
  case lookup hostName myHosts of
    Just host -> return host
    Nothing -> do
      liftIO . spawn $ "xmessage \"" ++ missingHostMsg hostName ++ "\""
      return myDefaultHost
    where
      missingHostMsg hostName = "The hostname '" ++ hostName ++
            "' is not specified in the xmonad config file.\n\n" ++
            "Please edit '~/.xmonad/xmonad.hs'."

-- Main

main = do
  host <- getHost
  xmonad $ myConfig host

myConfig host = defaultConfig
  { manageHook          = myManageHooks
  , logHook             = myLogHook host >> updatePointer (Relative 0.5 0.5)
  , layoutHook          = myLayout
  , handleEventHook     = fullscreenEventHook
  , modMask             = modifier host 
  , keys                = flip mkKeymap $ myKeys conf
  , workspaces          = map fst myWorkspaces
  , startupHook         = setWMName "LG3D" >>
                          checkKeymap conf (myKeys conf)
  , focusedBorderColor  = bgColorCurrent
  , normalBorderColor   = "#000000"
  }
  where conf = myConfig host


myManageHooks = composeOne
  [ isDialog -?> doCenterFloat
  ]

-- Layout

myLayout = (
            -- only show borders when more than one window is present
            smartBorders $
            
            -- make space for the dzen bars
            avoidStruts $

            -- ability to mirror or reflect the layout
            mkToggle (MIRROR ?? REFLECTX ?? EOT) $

            --MessageMap (swap2 Expand Shrink) $

            -- default layout
            tiled
           )

           -- fullscreen as an alternative layout
           ||| noBorders Full
  where
    -- tiled layout
    tiled        = mastered delta masterRatio column
    delta        = 3/100
    masterRatio  = 3/5

    -- column layout that only processes escaped Shrink/Expand messages
    -- (Both mastered and GeoColumn respond to Shrink/Expand messages)
    column       = ignore Shrink . ignore Expand . unEscape $ GeoColumn sideRatio
    sideRatio    = 1.6


-- Workspace formating

myLogHook host = dynamicLogWithPP $ defaultPP
  { ppTitle = shorten 80
  , ppCurrent = dzenColor fgColorCurrent bgColorCurrent . padding
  , ppVisible = dzenColor fgColorVisible bgColorVisible . clickable padding
  , ppHidden  = dzenColor fgColorHidden  bgColorHidden . clickable padding
  , ppHiddenNoWindows = dzenColor fgColorEmpty bgColorEmpty . clickable padding 
  , ppSep     = "  "
  , ppWsSep   = ""
  , ppOrder   = \(workspaces: layout: title:_) -> [workspaces, title]
  }
  where
    clickable :: (WorkspaceId -> String) -> WorkspaceId -> String
    clickable f ws = addClickEvent ws (f ws)

    padding :: WorkspaceId -> String
    padding = (' ':) . (++" ")

    addClickEvent :: WorkspaceId -> String -> String
    addClickEvent ws s = fromMaybe s $
      format <$> lookup (modifier host) [(mod4Mask, "super")
                                        ,(mod1Mask, "alt")]
             <*> lookup ws myWorkspaces
      where
        format keyMod key =
          "^ca(1,xdotool key " ++ keyMod ++ "+" ++ key ++ ")" ++ s ++ "^ca()"


-- keybindings

myKeys conf =
    -- launch programs
  [ ("M-t",  spawn (myTerminal ++ " >/dev/null"))
  , ("M-z",  spawn (myBrowser ++ " >/dev/null"))
  , ("M-\\", spawn (myLauncher ++ " >/dev/null"))

    -- window commands
  , ("M-c",       kill)
  , ("M-<Tab>",   windows S.focusDown)
  , ("M-S-<Tab>", windows S.focusUp)
  , ("M-a",       windows S.shiftMaster)

  , ("M-s", windows S.swapDown)
  , ("M-d", windows S.swapUp)
  , ("M-f", withFocused $ windows . S.sink)

    -- Layout commands
  , ("M-<Space>",   sendMessage NextLayout)
  , ("M-S-<Space>", setLayout . Layout . XMonad.layoutHook $ conf)
  , ("M-h",         sendMessage Shrink)
  , ("M-l",         sendMessage Expand)
  , ("M-k",         sendMessage $ escape Shrink)
  , ("M-j",         sendMessage $ escape Expand)
  , ("M-b",         sendMessage $ IncMasterN 1)
  , ("M-n",         sendMessage . IncMasterN $ -1)
  , ("M-m",         sendMessage $ Toggle MIRROR)
  , ("M-x",         sendMessage $ Toggle REFLECTX)

    -- Audio controll
  , ("<XF86AudioMute>",        spawn "~/scripts/audio.sh toggle")
  , ("<XF86AudioLowerVolume>", spawn "~/scripts/audio.sh 3- unmute")
  , ("<XF86AudioRaiseVolume>", spawn "~/scripts/audio.sh 3+ unmute")

    -- Background change
  , ("M-<Right>", spawn "~/scripts/bgupdate.sh")
  , ("M-<Left>",  spawn "~/scripts/bgupdate.sh -1")

    -- Termination commands
  , ("C-<Esc>", io (exitWith ExitSuccess))
  , ("M-<Esc>", restart "xmonad" True)
  ]
  ++

  -- mod-workspaceKey(N), Switch to workspace N
  -- mod-shift-workspaceKey(N), Move client to workspace N
  [ ("M-" ++ modifier ++ wsKey, windows $ operation wsName)
    | (wsName, wsKey) <- myWorkspaces
    , (operation, modifier) <- [(S.greedyView, ""), (S.shift, "S-")]
  ]

  ++
  -- mod-{1,2,3}, Switch to physical/Xinerama screens 1, 2, or 3
  -- mod-shift-{1,2,3}, Move client to screen 1, 2, or 3
  [ ("M-" ++ modifier ++ key, screenWorkspace sc >>=
                              flip whenJust (windows . operation))
    | (key, sc) <- zip ["1", "2", "3"] [0..]
    , (operation, modifier) <- [(S.view, ""), (S.shift, "S-")]
  ]




--
-- Message transformation layout
--

instance Eq Resize where
  Shrink == Shrink = True
  Expand == Expand = True
  _      == _      = False

data MessageMap m l w = MessageMap (m -> m) (l w)

instance Show (l w) => Show (MessageMap m l w) where
  show (MessageMap f l) = "MessageMap (" ++ show l ++ ")"

instance Read (l w) => Read (MessageMap m l w) where
  readsPrec _ _ = []

instance (Message m, LayoutClass l w) => LayoutClass (MessageMap m l) w where
  runLayout ws@(S.Workspace _ (MessageMap f l') _) r =
    second (MessageMap f <$>) <$> runLayout (ws { S.layout = l' }) r

  handleMessage l@(MessageMap f l') sm = (MessageMap f <$>) <$> handleMessage l' msg
      where
        msg = case fromMessageAs sm l of
                Just m -> SomeMessage $ f m
                Nothing -> sm
        fromMessageAs :: Message m' => SomeMessage -> MessageMap m' l w -> Maybe m'
        fromMessageAs a _ = fromMessage a

swap2 :: Eq m => m -> m -> m -> m
swap2 a b c = if a == c then b else
              if b == c then a else
                 c

--data MIRROR_MSG = MIRROR_MSG deriving (Read, Show, Eq, Typeable)

--
-- Bug free definition of the Column layout (removed rounding errors)
-- 


data GeoColumn a = GeoColumn Float deriving (Read,Show)

instance LayoutClass GeoColumn a where
    pureLayout = columnLayout
    pureMessage = columnMessage

columnMessage :: GeoColumn a -> SomeMessage -> Maybe (GeoColumn a)
columnMessage (GeoColumn q) m = fmap resize (fromMessage m)
    where resize Shrink = GeoColumn (max 1 (q/1.1))
          resize Expand = GeoColumn (q*1.1)

columnLayout :: GeoColumn a -> Rectangle -> S.Stack a -> [(a,Rectangle)]
columnLayout (GeoColumn q) rect@(Rectangle x y w h) stack = zip ws rects
    where ws = S.integrate stack
          n = length ws
          heights' = map (xn n h q) [1..n]
          delta = fromIntegral (sum heights') - fromIntegral h
          heights = if delta <= 0
            then zipWith (+) heights' (take (-delta) [1,1..] ++ [0,0..])
            else zipWith (-) heights' (take (n - delta) [0,0..] ++ [1,1..])
          ys = map fromIntegral $ scanl (+) 0 heights
          rects = map (mkRect rect) $ zip heights ys

mkRect :: Rectangle -> (Dimension,Position) -> Rectangle
mkRect (Rectangle xs ys ws _) (h,y) = Rectangle xs (ys+y) ws h

xn :: Int -> Dimension -> Float -> Int -> Dimension
xn n h q k = if q==1
               then h `div` fromIntegral n
               else round ((fromIntegral h)*q^(n-k)*(1-q)/(1-q^n))


--
-- QuickCheck definitions
--

instance Arbitrary (GeoColumn l) where
  arbitrary = choose (1, 10) >>= \q -> return $ GeoColumn q

instance Arbitrary Rectangle where
  arbitrary = do
              w <- choose (1, 1000) :: Gen Int
              h <- choose (1, 1000) :: Gen Int
              x <- choose (1, 1000) :: Gen Int
              y <- choose (1, 1000) :: Gen Int
              return $ Rectangle (fromIntegral x) (fromIntegral y)
                                 (fromIntegral w) (fromIntegral h)

instance Arbitrary (S.Stack ()) where
  arbitrary = do
    n <- choose (1, 20) :: Gen Int
    return . fromJust . S.differentiate . take n $ repeat ()

-- | Property to check whether the rectangles in the GeoColumn layout are
-- continuous.
--
-- The GeoColumn layout ensures that the bottom coordinate of a rectangle is the
-- top coordinate of the next rectangle and that the first and last rectangle
-- are aligned with the surrounding rectangle
propContinuous :: GeoColumn a -> Rectangle -> S.Stack a -> Bool
propContinuous c r s = rect_y (head rectangles) : bottom_pos ==
                        top_pos ++ [rect_y r + fromIntegral (rect_height r)]
  where
    rectangles = map snd $ columnLayout c r s

    -- The top and bottom coordinates of each rectangle
    top_pos = map rect_y rectangles
    bottom_pos = map (\(Rectangle _ y _ h) -> y + fromIntegral h) rectangles

-- | Property to check whether the rectangles in the GeoColumn layout have
-- the same x and width as the provided rectangle.
propSide :: GeoColumn a -> Rectangle -> S.Stack a -> Bool
propSide c r s = all (== rect_x r) (map rect_x rectangles) &&
                 all (== rect_width r) (map rect_width rectangles)
  where
    rectangles = map snd $ columnLayout c r s

-- | Property to check whether the layout GeoColumn conforms to its
-- specification.
propGeoColumn :: GeoColumn a -> Rectangle -> S.Stack a -> Bool
propGeoColumn c r s = propContinuous c r s && propSide c r s

runTest :: IO Result
runTest = quickCheckWithResult args prop1
  where
    prop1 :: GeoColumn () -> Rectangle -> S.Stack () -> Bool
    prop1 = propGeoColumn
    args = stdArgs { maxSuccess = 2000 }
